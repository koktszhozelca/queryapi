const express = require("express");
const app = express();

app.use(express.static("public"))

app.use("/api", (req, res)=>{
  res.send("API data, reply the query of " + req.query.name + " with age " + req.query.age);
})

app.listen(3000, ()=>{
  console.log("Server listen on port 3000");
})
