# QueryAPI DEMO

## Steps

* 1. git clone https://gitlab.com/koktszhozelca/queryapi.git
* 2. cd queryapi
* 3. npm i
* 4. node index.js
* 5. browse http://localhost:3000

## Code

* Query code in index.html
* API code in index.js

## Query code
```javascript
$("#btnQuery").click(()=>{
    var param = {
      name: $("#name").val(),
      age: $("#age").val()
    }
    $.get("/api", param, (data)=>{
      $("#response").html(data)
    })
})
```

## API Code 

```javascript
const express = require("express");
const app = express();

app.use(express.static("public"))

app.use("/api", (req, res)=>{
  res.send("API data, reply the query of " + req.query.name + " with age " + req.query.age);
})

app.listen(3000, ()=>{
  console.log("Server listen on port 3000");
})
```